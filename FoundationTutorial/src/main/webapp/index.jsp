<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Foundation 5</title>
<!-- If you are using the CSS version, only link these 2 files, you may add app.css to use for your overrides if you like -->
<link rel="stylesheet" href="<c:url value="/css/normalize.css"/>">
<link rel="stylesheet" href="<c:url value="/css/foundation.css"/>">

<script type="text/javascript" src="<c:url value="/js/vendor/modernizr.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/angular.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/angular-ui-router.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/angular-resource.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/app.js"/>"></script>
<!-- foundation datepicker start -->
<link rel="stylesheet" href="<c:url value="/css/foundation-datepicker.css"/>">
<script type="text/javascript">
	function fDatePicker() {
		$('#DOB').fdatepicker();
	}
</script>
<!-- foundation datepicker end -->
</head>
<body ng-app='MyTutorialApp'>
	<!-- body content here -->
	<nav class="top-bar" data-topbar>
	  <ul class="title-area">
	    <li class="name">
	      <h1><a href="#">Foundation Tutorial</a></h1>
	    </li>
	     <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
	    <li class="toggle-topbar menu-icon"><a href=""><span>Menu</span></a></li>
	  </ul>

	  <section class="top-bar-section">
	    <!-- Left Nav Section -->
	    <ul class="left">
	      <li><a href="#/home">Home</a></li>
	      <li><a href="#/about">About</a></li>
	      <li><a href="#/contact">Contact</a></li>
	    </ul>
	  </section>
  	</nav>

	<!-- this is where content will be injected -->
	<div ui-view ng-controller="EmployeeController"></div>
	<script type="text/javascript" src="<c:url value="/js/vendor/jquery.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/vendor/fastclick.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/foundation.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/foundation-datepicker.js"/>"></script>
	<script type="text/javascript">
		$(document).foundation();
	</script>
</body>
</html>