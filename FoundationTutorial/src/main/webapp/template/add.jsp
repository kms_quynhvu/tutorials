<div class="small-12 columns">
	<h3 ng-if="$state.includes('add')">Add Employee</h3>
	<h3 ng-if="$state.includes('edit')">Edit Employee</h3>
	<form data-abide role="form" name="addForm" ng-submit="addOrEditEmployee()">
		<div class="row">
			<div class="small-12 columns">
				<label for="fName">First Name</label>
				<input type="text" name="fName" id="fName" placeholder="First Name" ng-model="emp.firstName" onclick="fDatePicker();" required pattern="[a-zA-Z]+"/>
				<small class="error">First Name is required and must be a string.</small>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<label for="lName">Last Name</label>
				<input type="text" name="lName" id="lName" placeholder="Last Name" ng-model="emp.lastName" required/>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<label for="email">Email</label>
				<input type="email"	name="email" id="email" placeholder="Email" ng-model="emp.email" required/>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
			  <label>Gender</label>
			  <input type="radio" name="gender" id="male" value="true" ng-model="emp.gender" ng-checked="emp.gender"><label>Male</label>
			  <input type="radio" name="gender" id="female" value="false" ng-model="emp.gender" ng-checked="!emp.gender"><label>Female</label>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<label for="DOB">Birth Date</label>
		        <input type="date" name="DOB" id="DOB" ng-model="emp.birthDate" data-date-format="yyyy-mm-dd" required />
		    </div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<label for="info">More Info</label>
				<textarea rows="3" name="info" id="info" placeholder="Info" ng-model="emp.info" required></textarea>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<label for="image">Image</label>
				<input type="file" name="image" id="image" ng-model="emp.image" placeholder="Image" required/>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<button type="submit" class="button round">Submit</button>
				<a href="" ng-click="cancelAddOrEdit()" class="button round alert">Cancel</a>
			</div>
		</div>
	</form>
</div>