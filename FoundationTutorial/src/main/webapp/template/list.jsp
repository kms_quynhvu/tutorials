<div class="small-12 columns">
	<h3 class="sub-header">Employees</h3>
	<div ng-if="$state.includes('list')" style="text-align: right;">
		<a class="button round" ng-click="navToAddEmp()">Add</a>
		<a class="button round alert" href="#/grid" ng-click="viewAs('grid')">Grid</a>
	</div>

	<table style="width: 100%">
		<thead>
			<tr>
				<th ng-repeat="header in headers">
			        <sort-by onsort="onSort" sortdir="filterCriteria.sortDir" sortedby="filterCriteria.sortedBy" sortvalue="{{header.value}}">
			        	{{ header.title }}
			        </sort-by>
			     </th>
			     <th></th>
			     <th>
					<select class="form-control" ng-model="filterCriteria.pageSize" ng-options="pageSize for pageSize in [2,5,10,50]" title="Page Size" required></select>
			     </th>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat='emp in employees.content'>
				<td>{{emp.firstName}}</td>
				<td>{{emp.lastName}}</td>
				<td>{{emp.email}}</td>
				<td>{{emp.gender ? 'Male' : 'Female'}}</td>
				<td>{{emp.birthDate | date:'yyyy-MM-dd'}}</td>
				<td><img src="img/{{emp.image}}" width="150px" height="150px"/></td>
				<td>					
					<a class="button dropdown round" data-dropdown="drop{{emp.firstName}}{{emp.lastName}}" onclick="$(document).foundation();">Action</a>
					<ul id="drop{{emp.firstName}}{{emp.lastName}}" class="f-dropdown" data-dropdown-content>
					  <li><a ng-click="selectEmpForEdit(emp);">Edit</a></li>
					  <li><a ng-click="selectEmp(emp);" data-reveal-id="myModal">Delete</a></li>
					</ul>		
					
					<!-- Model -->
					<div id="myModal" class="reveal-modal tiny" data-reveal>
						<h2>Confirmation</h2>
						<p class="lead">Are you sure you want to delete?</p>
						<p>
							<a class="button round alert" ng-click="deleteSelectedEmp()" onclick="$('a.close-reveal-modal').trigger('click');">Delete</a>
							<a class="button round" onclick="$('a.close-reveal-modal').trigger('click');">Cancel</a>
						</p>	
						<a class="close-reveal-modal">&#215;</a>
					</div>
				</td>
			</tr>
		</tbody>
	</table>

    <ul class="pagination">
       <li>
       	<a href="" ng-click="selectPage(0)">&laquo;</a>
       </li>
       <li ng-class="{current: isSelectedPage(n)}" ng-repeat="n in [] | range:employees.totalPages">
       	<a href="" ng-click="selectPage(n)" ng-bind="n + 1"></a>
       </li>
       <li>
       	<a href="" ng-click="selectPage(employees.totalPages - 1)">&raquo;</a>
       </li>
   	</ul>

	<div class="progress round">
		<span class="meter" style="width: {{ ((filterCriteria.pageNumber + 1) / employees.totalPages) * 100 }}%;"></span>
	</div>
</div>