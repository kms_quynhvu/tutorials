<div ng-click="sort(sortvalue)" style="cursor: pointer;">
  <span ng-transclude></span>
  <span ng-show="sortedby === sortvalue">
    <i ng-class="{true: 'glyphicon glyphicon-arrow-down', false: 'glyphicon glyphicon-arrow-up'}[sortdir === 0]"></i>
  </span>
</div>