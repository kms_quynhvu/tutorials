<div class="small-12 columns">
	<h3>Employees</h3>
	<div ng-if="$state.includes('grid')" style="text-align: right;">
		<a class="button round" ng-click="navToAddEmp()">Add</a>
		<a class="button round alert" href="#/list" ng-click="viewAs('list')">List</a>
	</div>

	<table style="width: 100%">
		<thead>
			<tr>
				<th ng-repeat="header in headers">
			        <sort-by onsort="onSort" sortdir="filterCriteria.sortDir" sortedby="filterCriteria.sortedBy" sortvalue="{{header.value}}">
			        	{{ header.title }}
			        </sort-by>
			     </th>
			     <th></th>
			     <th>
					<select class="form-control" ng-model="filterCriteria.pageSize" ng-options="pageSize for pageSize in [2,5,10,50]" title="Page Size" required></select>
			     </th>
			</tr>
		</thead>
	</table>

	<ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
		<li ng-repeat="emp in employees.content">
			<img src="img/{{emp.image}}" height="220px" width="220px;"></img>
			<h3>{{emp.firstName | uppercase}} {{emp.lastName}}</h3>
			{{emp.email}}<br/>
			{{emp.gender ? 'Male' : 'Female'}}<br/>
			{{emp.birthDate | date:'yyyy-MM-dd'}}<br/>
			{{emp.info}}<br/>
			<a class="button tiny round" ng-click="selectEmpForEdit(emp)">Edit</a>
			<a class="button tiny round alert" ng-click="selectEmp(emp);" onclick="$(document).foundation();" data-reveal-id="myModal{{emp.firstName}}{{emp.lastName}}">Delete</a>				
					
			<!-- Model -->
			<div id="myModal{{emp.firstName}}{{emp.lastName}}" class="reveal-modal tiny" data-reveal>
				<h2>Confirmation</h2>
				<p class="lead">Are you sure you want to delete?</p>
				<p>
					<a class="button round alert" ng-click="deleteSelectedEmp()" onclick="$('a.close-reveal-modal').trigger('click');">Delete</a>
					<a class="button round" onclick="$('a.close-reveal-modal').trigger('click');">Cancel</a>
				</p>	
				<a class="close-reveal-modal">&#215;</a>
			</div>
		</li>
	</ul>

    <ul class="pagination">
       <li>
       	<a href="" ng-click="selectPage(0)">&laquo;</a>
       </li>
       <li ng-class="{current: isSelectedPage(n)}" ng-repeat="n in [] | range:employees.totalPages">
       	<a href="" ng-click="selectPage(n)" ng-bind="n + 1"></a>
       </li>
       <li>
       	<a href="" ng-click="selectPage(employees.totalPages - 1)">&raquo;</a>
       </li>
   	</ul>

	<div class="progress round">
		<span class="meter" style="width: {{ ((filterCriteria.pageNumber + 1) / employees.totalPages) * 100 }}%;"></span>
	</div>

</div>
