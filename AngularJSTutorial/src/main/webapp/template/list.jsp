<div class="col-md-12">
	<h3 class="sub-header">Employees</h3>
	<div class="row">
		<div class="col-md-12" style="text-align: right;">
			<p ng-if="$state.includes('list')">
				<a class="btn btn-info btn-lg glyphicon glyphicon-plus" ng-click="navToAddEmp()"></a>
				<a class="btn btn-primary btn-lg glyphicon glyphicon-th" href="#/grid" ng-click="viewAs('grid')"></a>
			</p>
		</div>
	</div>

	<div class="table-responsive panel panel-default">
		<table class="table table-striped">
			<thead>
				<tr>
					<th ng-repeat="header in headers">
				        <sort-by onsort="onSort" sortdir="filterCriteria.sortDir" sortedby="filterCriteria.sortedBy" sortvalue="{{header.value}}">
				        	{{ header.title }}
				        </sort-by>
				     </th>
				     <th></th>
				     <th>
						<select class="form-control" ng-model="filterCriteria.pageSize" ng-options="pageSize for pageSize in [2,5,10,50]" title="Page Size" required></select>
				     </th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat='emp in employees.content'>
					<td>{{emp.firstName}}</td>
					<td>{{emp.lastName}}</td>
					<td>{{emp.email}}</td>
					<td>{{emp.gender ? 'Male' : 'Female'}}</td>
					<td>{{emp.birthDate | date:'yyyy-MM-dd'}}</td>
					<td><img src="img/{{emp.image}}" class="img-responsive img-circle" width="150px" height="150px"/></td>
					<td>
						<div class="dropdown">
						  <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu{{emp.firstName}}{{emp.lastName}}" data-toggle="dropdown">
						    Action
						    <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu{{emp.firstName}}{{emp.lastName}}">
						    <li role="presentation"><a role="menuitem" tabindex="-1" ng-click="selectEmpForEdit(emp)" href=""><span class="glyphicon glyphicon-pencil"></span> Edit</a></li>
						    <li role="presentation" class="divider"></li>
						    <li role="presentation"><a role="menuitem" tabindex="-1" ng-click="selectEmp(emp)" data-toggle="modal" data-target="#myModal" href=""><span class="glyphicon glyphicon-remove"></span> Delete</a></li>
						  </ul>
						</div>
						<!-- Modal -->
						<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						        <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
						      </div>
						      <div class="modal-body">
						      	Are you sure you want to delete?
						      </div>
						      <div class="modal-footer">
						        <a class="btn btn-danger" ng-click="deleteSelectedEmp()" data-dismiss="modal">Delete</a>
						        <a class="btn btn-default" data-dismiss="modal">Cancel</a>
						      </div>
						    </div>
						  </div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
		<table class="table">
			<tbody>
			<tr>
				<td>
			        <ul class="pagination">
				       <li>
				       	<a href="" ng-click="selectPage(0)">&laquo;</a>
				       </li>
				       <li ng-class="{active: isSelectedPage(n)}" ng-repeat="n in [] | range:employees.totalPages">
				       	<a href="" ng-click="selectPage(n)" ng-bind="n + 1"></a>
				       </li>
				       <li>
				       	<a href="" ng-click="selectPage(employees.totalPages - 1)">&raquo;</a>
				       </li>
				   	</ul>
			     </td>
			</tr>
			</tbody>
		</table>
	</div>

	<div class="progress">
	  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ ((filterCriteria.pageNumber + 1) / employees.totalPages) * 100 }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ ((filterCriteria.pageNumber + 1) / employees.totalPages) * 100 }}%;">
	    {{ ((filterCriteria.pageNumber + 1) / employees.totalPages) * 100 }} %
	  </div>
	</div>

</div>