<div class="col-md-12">
	<h3 class="sub-header">Employees</h3>

	<div class="row">
		<div class="col-md-12" style="text-align: right;">
			<p ng-if="$state.includes('grid')">
				<a class="btn btn-info btn-lg glyphicon glyphicon-plus" ng-click="navToAddEmp()"></a>
				<a class="btn btn-primary btn-lg glyphicon glyphicon-th-list" href="#/list" ng-click="viewAs('list')"></a>
			</p>
		</div>
	</div>

	<div class="table-responsive panel panel-default">
		<table class="table table-striped">
			<thead>
				<tr>
					<th class="btn-primary" ng-repeat="header in headers">
				        <sort-by onsort="onSort" sortdir="filterCriteria.sortDir" sortedby="filterCriteria.sortedBy" sortvalue="{{header.value}}">
				        	{{ header.title }}
				        </sort-by>
				     </th>
				     <th class="btn-primary"></th>
				     <th class="btn-primary">
						<select class="form-control" ng-model="filterCriteria.pageSize" ng-options="pageSize for pageSize in [2,5,10,50]" title="Page Size" required></select>
				     </th>
				</tr>
			</thead>
		</table>
		<br/>
		<div class="col-lg-2 col-md-3 col-sm-4" ng-repeat="emp in employees.content">
			<div class="thumbnail" style="height: 450px;">
				<img src="img/{{emp.image}}" height="220px" width="220px;" class="img-responsive img-circle"/>
				<div class="caption">
					<h3>{{emp.firstName | uppercase}} {{emp.lastName}}</h3>
					<p>
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
						  <li class="active"><a href="" data-target="#brief{{emp.firstName}}{{emp.lastName}}" role="tab" data-toggle="tab">Brief</a></li>
						  <li><a href="" data-target="#details{{emp.firstName}}{{emp.lastName}}" role="tab" data-toggle="tab">Details</a></li>
						</ul>

						<!-- Tab panes -->
						<div class="tab-content">
						  <div class="tab-pane active" id="brief{{emp.firstName}}{{emp.lastName}}">
						  	<br/>
						  	{{emp.email}}<br/>
							{{emp.gender ? 'Male' : 'Female'}}<br/>
							{{emp.birthDate | date:'yyyy-MM-dd'}}
						  </div>
						  <div class="tab-pane" id="details{{emp.firstName}}{{emp.lastName}}">
						  	<br/>
						  	{{emp.info}}
						  </div>
						</div>
					</p>
					<p>
						<a class="btn btn-primary btn-xs" ng-click="selectEmpForEdit(emp)">Edit</a>
						<a class="btn btn-danger btn-xs" ng-click="selectEmp(emp)" data-toggle="modal" data-target="#myModal">Delete</a>
						<!-- Modal -->
						<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						        <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
						      </div>
						      <div class="modal-body">
						      	Are you sure you want to delete?
						      </div>
						      <div class="modal-footer">
						        <a class="btn btn-danger" ng-click="deleteSelectedEmp()" data-dismiss="modal">Delete</a>
						        <a class="btn btn-default" data-dismiss="modal">Cancel</a>
						      </div>
						    </div>
						  </div>
						</div>
					</p>
				</div>
			</div>
		</div>
		<table class="table">
			<tbody>
			<tr>
				<td>
			        <ul class="pagination">
				       <li>
				       	<a href="" ng-click="selectPage(0)">&laquo;</a>
				       </li>
				       <li ng-class="{active: isSelectedPage(n)}" ng-repeat="n in [] | range:employees.totalPages">
				       	<a href="" ng-click="selectPage(n)" ng-bind="n + 1"></a>
				       </li>
				       <li>
				       	<a href="" ng-click="selectPage(employees.totalPages - 1)">&raquo;</a>
				       </li>
				   	</ul>
			     </td>
			</tr>
			</tbody>
		</table>
	</div>

	<div class="progress">
	  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ ((filterCriteria.pageNumber + 1) / employees.totalPages) * 100 }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ ((filterCriteria.pageNumber + 1) / employees.totalPages) * 100 }}%;">
	    {{ ((filterCriteria.pageNumber + 1) / employees.totalPages) * 100 }} %
	  </div>
	</div>
</div>
