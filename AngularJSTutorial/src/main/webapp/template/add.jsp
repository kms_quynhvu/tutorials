<div class="col-md-12">
	<h3 class="sub-header" ng-if="$state.includes('add')">Add Employee</h3>
	<h3 class="sub-header"  ng-if="$state.includes('edit')">Edit Employee</h3>
	<div class="panel panel-default">
	  	<div class="panel-body">
			<form class="form-horizontal" role="form" name="addForm" ng-submit="addForm.$valid && addOrEditEmployee()" novalidate>
				<div class="form-group" ng-class="{ 'has-error' : addForm.fName.$invalid && (!addForm.fName.$pristine) }">
					<label class="col-sm-2 control-label" for="fName">First Name</label>
					<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon">T</span>
							<input type="text" class="form-control" name="fName" id="fName" placeholder="First Name" ng-model="emp.firstName" required/>
						</div>
						<p ng-show="addForm.fName.$invalid && (!addForm.fName.$pristine)" class="help-block">First Name is required !</p>
					</div>
				</div>

				<div class="form-group" ng-class="{ 'has-error' : addForm.lName.$invalid && (!addForm.lName.$pristine) }">
					<label class="col-sm-2 control-label" for="lName">Last Name</label>
					<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon">T</span>
							<input type="text" class="form-control" name="lName" id="lName" placeholder="Last Name" ng-model="emp.lastName" required/>
						</div>
						<p ng-show="addForm.lName.$invalid && (!addForm.lName.$pristine)" class="help-block">Last Name is required !</p>
					</div>
				</div>

				<div class="form-group" ng-class="{ 'has-error' : addForm.email.$invalid && (!addForm.email.$pristine) }">
					<label class="col-sm-2 control-label" for="email">Email</label>
					<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
							<input type="email"	class="form-control" name="email" id="email" placeholder="Email" ng-model="emp.email" required/>
						</div>
						<p ng-show="addForm.email.$invalid && (!addForm.email.$pristine)" class="help-block">Enter a valid email !</p>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Gender</label>
					<div class="radio col-sm-10">
					  <label>
					    <input type="radio" name="gender" id="male" value="true" ng-model="emp.gender" ng-checked="emp.gender">
					    Male
					  </label>
					  <label>
					    <input type="radio" name="gender" id="female" value="false" ng-model="emp.gender" ng-checked="!emp.gender">
					    Female
					  </label>
					</div>
				</div>

				<div class="form-group" ng-class="{ 'has-error' : addForm.DOB.$invalid && (!addForm.DOB.$pristine) }">
					<label class="col-sm-2 control-label" for="DOB">Birth Date</label>
					<div class="dropdown col-sm-10">
				      <a class="dropdown-toggle" id="dropdown2" role="button" data-toggle="dropdown" >
				        <div class="input-group">
				        	<input type="date" class="form-control" name="DOB" id="DOB" ng-model="emp.birthDate" required />
				        	<span class="input-group-addon"><i class="glyphicon glyphicon-calendar" style="cursor: pointer;"></i></span>
				        </div>
				      </a>
				      <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
				        <datetimepicker data-ng-model="emp.birthDate" data-datetimepicker-config="{ dropdownSelector: '#dropdown2', startView:'day', minView:'day' }"/>
				      </ul>
				      <p ng-show="addForm.DOB.$invalid && (!addForm.DOB.$pristine)" class="help-block">Birth Date is required !</p>
				    </div>
				</div>

				<div class="form-group" ng-class="{ 'has-error' : addForm.info.$invalid && (!addForm.info.$pristine) }">
					<label class="col-sm-2 control-label" for="info">More Info</label>
					<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon">T</span>
							<textarea class="form-control" rows="3" name="info" id="info" placeholder="Info" ng-model="emp.info" required></textarea>
						</div>
						<p ng-show="addForm.info.$invalid && (!addForm.info.$pristine)" class="help-block">Info is required !</p>
					</div>
				</div>

				<div class="form-group" ng-class="{ 'has-error' : addForm.image.$invalid && (!addForm.image.$pristine) }">
					<label class="col-sm-2 control-label" for="image">Image</label>
					<div class="col-sm-10">
						<input type="file" name="image" id="image" ng-model="emp.image" placeholder="Image" required/>
						<p ng-show="addForm.image.$invalid && (!addForm.image.$pristine)" class="help-block">Select a image!</p>
					</div>
				</div>

				<div class="form-group">
		    		<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-primary">Submit</button>
						<a href="" ng-click="cancelAddOrEdit()" class="btn btn-warning">Cancel</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
