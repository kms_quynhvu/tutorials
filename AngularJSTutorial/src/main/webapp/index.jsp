<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>AngularJS Tutorial</title>

<script type="text/javascript" src="<c:url value="/js/angular.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/angular-ui-router.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/angular-resource.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/app.js"/>"></script>
<!-- Bootstrap core CSS -->
<link href="<c:url value="/css/bootstrap.min.css"/>" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<c:url value="/css/dashboard.css"/>" rel="stylesheet">
<link href="<c:url value="/css/datetimepicker.css"/>" rel="stylesheet">
</head>

<body ng-app='MyTutorialApp'>
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#/list">AngularJS Tutorial</a>
			</div>
			<div class="collapse navbar-collapse">
	          <ul class="nav navbar-nav">
	            <li class="active"><a href="#">Home</a></li>
	            <li><a href="#/about">About</a></li>
	            <li><a ui-sref="contact">Contact</a></li>
	          </ul>
	        </div><!--/.nav-collapse -->
		</div>
	</div>

	<div class="container-fluid" ng-controller="EmployeeController">
		<div class="row">
			<!-- angular template -->
			<!-- this is where content will be injected -->
			<div ui-view></div>
		</div>
	</div>
	
	<script type="text/javascript" src="<c:url value="/js/moment.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/jquery.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/bootstrap.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/datetimepicker.js"/>"></script>
</body>
</html>
