var app = angular.module('MyTutorialApp', [ 'ui.router', 'ngResource', 'ui.bootstrap.datetimepicker']);

// We can use state or router
// http://scotch.io/tutorials/javascript/angular-routing-using-ui-router

app.config(function($stateProvider, $urlRouterProvider) {
	// in home page or pagination
	$urlRouterProvider.otherwise('/list');

	$stateProvider
	.state('list', {
        url: '/list',
        templateUrl: 'template/list.jsp'
    })
    .state('grid', {
        url: '/grid',
        templateUrl: 'template/grid.jsp'
    })
    .state('add', {
    	url: '/add',
    	templateUrl: 'template/add.jsp'
    })
	.state('edit', {
    	url: '/edit',
    	templateUrl: 'template/add.jsp'
    })
	.state('about', {
    	url: '/about',
    	views: {
    		'': {
    			templateUrl: 'template/about.jsp'
    		},
    		'left@about': {
    			templateUrl: 'template/about_left.jsp'
    		},
    		'right@about': {
    			templateUrl: 'template/about_right.jsp'
    		}
    	}

    })
	.state('contact', {
    	url: '/contact',
    	templateUrl: 'template/contact.jsp'
    });
});

app.factory("EmployeeFactory", function ($resource) {
    return $resource("/RESTTutorial/employee/:id", {id: "@id"}, {
    	// method paging
    	paging: {
    		method: 'GET'
    	}
    	// Add more function here
    });
});

app.filter('range', function() {
	  return function(input, total) {
		    total = parseInt(total);
		    for (var i=0; i<total; i++) {
		    	input.push(i);
		    }
		    return input;
	  };
});

app.directive('sortBy', function () {
  return {
	    templateUrl: 'template/sort-by.jsp',
	    restrict: 'E',
	    transclude: true,
	    replace: true,
	    scope: {
	      sortdir: '=',
	      sortedby: '=',
	      sortvalue: '@',
	      onsort: '='
	    },
	    link: function (scope, element, attrs) {
	      scope.sort = function () {
	        if (scope.sortedby === scope.sortvalue)
	          scope.sortdir = (scope.sortdir === 0 ? 1 : 0);
	        else {
	          scope.sortedby = scope.sortvalue;
	          scope.sortdir = 0;
	        }
	        scope.onsort(scope.sortedby, scope.sortdir);
	      }
	    }
	  };
});

app.controller("EmployeeController", ['$scope', '$state', '$filter', 'EmployeeFactory', function($scope, $state, $filter, EmployeeFactory) {
	$scope.$state = $state;
	// $scope.totalPages = 0;
	$scope.headers = [
      {
        title: 'First Name',
        value: 'firstName'
      },
      {
        title: 'Last Name',
        value: 'lastName'
      },
      {
        title: 'Email',
        value: 'email'
      },
      {
        title: 'Gender',
        value: 'gender'
      },
      {
        title: 'DOB',
        value: 'birthDate'
      }
    ];

	//default criteria that will be sent to the server
	$scope.filterCriteria = {
	   pageSize: 2,
	   pageNumber: 0,
	   sortDir: 0,
	   sortedBy: 'firstName',
	   // View as 'list' or 'grid'
	   viewAs: 'list' // default is 'list'
	};

	//The function that is responsible of fetching the result from the server and setting the grid to the new result
	$scope.fetchResult = function () {
		$scope.employees = EmployeeFactory.paging({
	 		pageSize: 	$scope.filterCriteria.pageSize,
	 		pageNum: 	$scope.filterCriteria.pageNumber,
	 		sortBy: 	$scope.filterCriteria.sortedBy,
	 		direction:	$scope.filterCriteria.sortDir
 		});
	};

	//called when navigate to another page in the pagination
	$scope.selectPage = function (page) {
	  $scope.filterCriteria.pageNumber = page;
	  $scope.fetchResult();
	};

	$scope.isSelectedPage = function (page) {
	  return $scope.filterCriteria.pageNumber === page;
	};

	//call back function that we passed to our custom directive sortBy, will be called when clicking on any field to sort
	$scope.onSort = function (sortedBy, sortDir) {
	  $scope.filterCriteria.sortDir = sortDir;
	  $scope.filterCriteria.sortedBy = sortedBy;
	  $scope.filterCriteria.pageNumber = 0;
	  $scope.fetchResult().then(function () {
	    //The request fires correctly but sometimes the ui doesn't update, that's a fix
	    $scope.filterCriteria.pageNumber = 0;
	  });
	};

	//manually select a page to trigger an ajax request to populate the grid on page load
	$scope.selectPage(0);

	$scope.$watch('emp.birthDate', function (newValue) {
	    $scope.emp.birthDate = $filter('date')(newValue, 'yyyy-MM-dd'); // Or whatever format your real model should use
	});

	$scope.$watch('filterCriteria.pageSize', function (newValue) {
	    $scope.filterCriteria.pageSize = newValue;
	    $scope.fetchResult();
	    $scope.selectPage(0);
	});

	// For delete employee
	$scope.deleteSelectedEmp = function() {
		EmployeeFactory.delete({id: $scope.emp.id}, function() {
			init();
	    });
	};

	$scope.selectEmp = function(emp) {
		$scope.emp = emp;
	};

	// For edit employee
	$scope.selectEmpForEdit = function(emp) {
		$scope.selectEmp(emp);
		$state.transitionTo('edit');
	};

	// For create new employee
	$scope.navToAddEmp = function() {
		$scope.emp = [];
		$state.transitionTo('add');
	};

	// Action for add or edit employee
	$scope.addOrEditEmployee = function() {
		// for the case add new employee
		var emp = new EmployeeFactory($scope.emp);

		emp.$save({}, function() {
			$state.transitionTo($scope.filterCriteria.viewAs);
			init();
	    });
	};

	$scope.cancelAddOrEdit = function() {
		$state.transitionTo($scope.filterCriteria.viewAs);
		init();
	};

	$scope.viewAs = function(newValue) {
		$scope.filterCriteria.viewAs = newValue;
	};


	function init() {
		// Get List of Employees
		$scope.fetchResult();
	}
}]);