package com.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.rest.model.Employee;
import com.rest.repository.EmployeeRepository;
import com.rest.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	
	@Autowired
	EmployeeRepository employeeRepository;

	@Override
	public Employee saveOrUpdate(Employee emp) {
		return employeeRepository.save(emp);
	}

	@Override
	public void delete(String id) {
		employeeRepository.delete(findOne(id));
	}

	@Override
	public Employee findOne(String id) {
		return employeeRepository.findOne(id);
	}

	@Override
	public Page<Employee> findWithPagination(int pageNum, int pageSize, String sortBy,	Direction direction) {
		return employeeRepository.findAll(new PageRequest(pageNum, pageSize, direction, sortBy));
	}
}
