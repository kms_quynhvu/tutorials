package com.rest.service.impl;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class LoggingService {
    private static final Logger LOGGER_PERF = Logger.getLogger("LOGGER_PERF");
    private Properties prop = new Properties();
    private double thresholdOfTimeToLog = 60000;
    private boolean turnOnDebug = false;

    public LoggingService() {
        try {
            prop.load(getClass().getClassLoader().getResourceAsStream("performances.properties"));
        } catch (IOException e) {
            LOGGER_PERF.error("Error getting properties", e);
        }
        thresholdOfTimeToLog = Double.parseDouble(prop.getProperty("THRESHOLD_OF_TIME_TO_LOG", "60000"));
        turnOnDebug = Boolean.parseBoolean(prop.getProperty("TURN_ON_DEBUG", "false"));
    }    

    @Around("execution(* com.rest.service.*Service.*(..))")
    public Object aroundAdviceForService(ProceedingJoinPoint pjp) throws Throwable {
        Object retVal = null;
        if (turnOnDebug) {
            Object[] args = pjp.getArgs();
            StringBuilder listArgs = new StringBuilder();
            StringBuilder listArgsValues = new StringBuilder();

            for (Object obj : args) {
                listArgs.append(obj.getClass().getName()).append(", ");
                listArgsValues.append(obj.toString()).append(", ");
            }

            if (StringUtils.isNotBlank(listArgs.toString())) {
                listArgs.delete(listArgs.lastIndexOf(","), listArgs.length());
            }

            if (StringUtils.isNotBlank(listArgsValues.toString())) {
                listArgsValues.delete(listArgsValues.lastIndexOf(","), listArgsValues.length());
            }

            String declaringTypeName = pjp.getSignature().getDeclaringTypeName();
            String signature = pjp.getSignature().getName();

            StringBuilder buffer = new StringBuilder();
            buffer.append(declaringTypeName).append(".").append(signature).append("(").append(listArgs).append(")");

            LOGGER_PERF.debug("PERF SERV START: " + buffer.toString());
            LOGGER_PERF.debug("Arguments: [" + listArgsValues + "]");
            double lStartTime = new Date().getTime(); // start time
            retVal = pjp.proceed();
            double lEndTime = new Date().getTime(); // end time
            double difference = lEndTime - lStartTime;
            if (difference >= thresholdOfTimeToLog) {
                LOGGER_PERF.debug("PERF SERV Elapsed: " + difference + " ms, " + difference / (1000 * 60) + " m");
            }
            LOGGER_PERF.debug("PERF SERV END: " + buffer.toString());
        } else {
            retVal = pjp.proceed();
        }
        return retVal;
    }
}
