package com.rest.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;

import com.rest.model.Employee;

public interface EmployeeService {
	public Employee findOne(String id);
	public Employee saveOrUpdate(Employee emp);
	public void delete(String id);
	public Page<Employee> findWithPagination(int pageNum, int pageSize, String sortBy,	Direction direction);
}
